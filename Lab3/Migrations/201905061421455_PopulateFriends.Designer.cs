// <auto-generated />
namespace Lab3.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class PopulateFriends : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(PopulateFriends));
        
        string IMigrationMetadata.Id
        {
            get { return "201905061421455_PopulateFriends"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
