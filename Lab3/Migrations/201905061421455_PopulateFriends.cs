namespace Lab3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateFriends : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FriendModels", "Ime", c => c.String(nullable: false));
            AlterColumn("dbo.FriendModels", "MestoZiveenje", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FriendModels", "MestoZiveenje", c => c.String());
            AlterColumn("dbo.FriendModels", "Ime", c => c.String());
        }
    }
}
