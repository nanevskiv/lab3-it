﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Lab3.Models
{
    public class FriendModel
    {
        [Required]
        [Range(0,200)]
        [Display(Name = "Friend Id")]
        public virtual int Id { get; set; }
        [Required]
        [Display(Name = "Friend Name")]
        public virtual string Ime { get; set; }
        [Required]
        [Display(Name = "Place")]
        public virtual string MestoZiveenje { get; set; }

    }
}